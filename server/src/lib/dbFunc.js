require("dotenv").load();
const axios = require("axios");
const db = require("./db");

const IGDB = db.get("IGDB");

const igdbOps = {
  getGames: async (limit, ...id) => {
    let data = `fields
                cover,
                name,
                first_release_date,
                total_rating,
                total_rating_count,
                platforms,
                franchise,
                status,
                summary,
                genres,
                involved_companies,
                game_engines;`;

    if (limit !== undefined && limit !== null) {
      data += `limit ${limit};`;
    } else if (id !== undefined && id !== null) {
      data += `where id = (${id});`;
    }

    axios({
      url: "https://api-v3.igdb.com/games",
      method: "POST",
      headers: {
        Accept: "application/json",
        "user-key": process.env.IGDB_API_KEY
      },
      data
    })
      .then(response => {
        console.log(response.data);
        return response.data;
      })
      .catch(err => {
        console.log(err);
      });
  }
};

module.exports = {
  IGDB: igdbOps
};
