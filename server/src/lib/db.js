require("dotenv").load();

const monk = require("monk");
const connStr = `${process.env.MDB_METHOD}://${process.env.MDB_USER}:${
  process.env.MDB_PASS
}@${process.env.MDB_HOST}/${process.env.MDB_DB}?${process.env.MDB_OPTIONS}`;

const db = monk(connStr);

module.exports = db;
