const { json, send } = require("micro");
const { router, get, post, patch, del } = require("microrouter");
const { IGDB } = require("./lib/dbFunc");

const status = (req, res) => send(res, 200, "OK");
const defaultRoute = (req, res) =>
  send(res, 200, "Mivo API | Default Response");
const notFound = (req, res) => send(res, 404, "Not Found");

// CRUD Event routes
const getGames = async (req, res) => send(res, 200, await IGDB.getGames(2));

const routes = [
  get("/", defaultRoute),
  get("/status", status),

  get("/getGames", getGames), // Returns Array Of Games From IGDB

  get("/*", notFound)
];

const injectHeaders = fn => (req, res) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.setHeader("Access-Control-Allow-Headers", "Content-Type");
  console.log(req.headers);
  fn(req, res);
};

module.exports = injectHeaders(router(...routes));
