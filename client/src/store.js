import axios from "axios";
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    games: [],
    selectedGames: [],
    limit: 5,
    searchQuery: ""
  },
  mutations: {
    addGames: (state, games) => (state.games = games.unshift(games)),
    addSelectedGame: (state, selectedGame) =>
      (state.selectedgGames = selectedGame),
    setLimit: (state, limit) => (state.limit = limit),
    setQuery: (state, searchQuery) => (state.searchQuery = searchQuery)
  },
  actions: {
    async fetchGames({ commit }) {
      const response = await axios.get(
        `${process.env.VUE_APP_SERVER}/getGames`
      );
      commit("addGames", response.data);
    },
    async SelectGame({ commit }, game) {
      commit("addSelectedGame", game);
    },
    setLimit({ commit }, limit) {
      commit("setLimit", limit);
    },
    setQuery({ commit }, searchQuery) {
      commit("setQuery", searchQuery);
    }
  }
});
